\part[author={Stéphane GALLAND},label={chap:overview}]{Overview of the Compilation Theory}

\begin{bibunit}[apalike]

\tableofcontentslide

\section{Introduction}

\begin{frame}{Introduction}
	\begin{description}
	\item Programming languages are notations for describing computations to people and to machines.
	\item All the software running on all the computers was written in some programming language.
	\item Before a program can be run, it first must be translated into a form in which it can be executed by a computer.
	\item The software systems that do this translation are called compilers.
	\item[Goal of this chapter] give an overview of a typical simple compiler.
	\item With this chapter you may understand the key points of language theory.
	\end{description}
\end{frame}

\section{Programming languages}

\tableofcontentslide[sectionstyle={show/shaded},subsectionstyle={show/show/hide},subsubsectionstyle={hide/hide/hide/hide}]

\subsection{Brief history}
\sidecite{Wexelblat.1981, Bergin.96}
\begin{frame}{A Brief History of Programming Languages}
	\begin{description}
	\item[1940's] machine language, sequences of 0's and 1's.
	\item[1950's] mnemonic assembly languages.
	\item[Later in 1950's] Fortran for scientific computation, Cobol for business data processing, Lisp for symbolic computation. ALGOL (ALGOrithmic Language) is the ancestor of the modern languages such as B, Pascal, Simula and C.
	\item[1960's and 1970's] Refinements in the 3GL \begin{itemize}
		\item APL: array programming and functional programming
		\item PL/I: merging the best concepts from Fortan and Cobol.
		\item Simula: first OO language, followed by Smalltalk
		\item C: operating system programming
		\item Prolog: first logic programming language
		\item ML: polymorphic type system on top of Lisp
		\end{itemize}
	\end{description}
\end{frame}

\sidenote{Figure by Matthew Hancock --- Complete figure on \url{moodle.utbm.fr}}
\figureslide[scale=1]{A Brief History of Programming Languages}{brief_history_of_languages}

\subsection{Classifications and types of programming languages}
\begin{frame}{Generations of Programming Languages}
	\begin{description}
	\item[1st Generation -- 1GL] Machine languages.
	\item[2nd Generation -- 2GL] Assembly languages.
	\item[3rd Generation -- 3GL] High-level languages (Fortran, Cobol, Lisp, C, C++, C\#, Java\dots)
	\item[4th Generation -- 4GL] languages designed for specific applications, like Nomad for report generation, SQL for database, Postscript for text formatting.
	\item[5th Generation -- 5GL] languages based on logic and constraints, like Prolog and OPS5.
	\end{description}
\end{frame}

\begin{frame}{Imperative or Declarative Language}
	\begin{block}{Imperative Languages}
	\begin{itemize}
	\item Imperative programs are specifying how a computation is to be done.
	\item Notion of program state and statements that change the state.
	\item \inlineexamples{C, C++, C\#, Java}.
	\end{itemize}
	\end{block}
	\begin{block}{Declarative Languages}
	\begin{itemize}
	\item Declarative programs are specifying what computation is to be done.
	\item Functional and logic-based languages.
	\item \inlineexamples{ML, Haskell, Prolog}.
	\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Object-oriented Language}
	\begin{itemize}
	\item \emph{Supports object-oriented programming.}
	\vfill
	\item Consists in building programs from a collection of objects that interact with one another.
	\end{itemize}
	\vfill
	\begin{examples}\begin{itemize}
		\item Simula 67, Smalltalk (the earliest major OO languages)
		\item C++, C\#, Java, Ruby\dots.
		\end{itemize}
	\end{examples}
\end{frame}

\begin{frame}{Scripting Language}
	\begin{itemize}
	\item \emph{Interpreted languages with high-level operators designed for ``gluing together'' computations.}
	\vfill
	\item Programs are much shorter than equivalent program written in other languages.
	\end{itemize}
	\vfill
	\begin{examples}
		Awk, Basic, JavaScript, Perl, PHP, Python, Ruby, Tcl, \dots
	\end{examples}
\end{frame}

\subsection{Basics of programming languages}

\tableofcontentslide[currentsection,currentsubsection]

\subsubsection{Definitions}
\sidecite{Sethi.1996, Scott.2006}
\begin{frame}[allowframebreaks]{Definitions}
	\begin{description}
	\item[Name] a string of characters that refers to a thing in the program.
	\item[Identifier] a string of characters that refers to an entity (data object, procedure, class, type). \begin{itemize}
		\item All identifiers are names; but not all names are identifiers
		\item \id{x.y} is a name but not an identifier, and \id{x} and \id{y} are identifiers.
		\end{itemize}
	\item[Variable] a particular location of the store of the values at run-time. A variable is denoted by a name. Each declaration of an identifier introduces a new variable.
	\item[Keyword] an identifier that has a particular meaning to the programming language.
	\end{description}
	%
	\framebreak
	%
	\begin{description}
	\item[Procedure] a subprogram that may be called.
	\item[Function] a procedure that may return a value of some type (the``return type'').
	\item[Method] a procedure or a function inside a class in object-oriented languages.
	\end{description}
	\begin{alertblock}{Caution}
	In the C-family languages, all the subprograms are functions; and a function is enabled to return nothing (\kw{void}).
	\end{alertblock}
	%
	\framebreak
	%
	\begin{description}
	\item[Declaration] tells us about the type of a thing. \begin{itemize}
		\item \inlineexample{\code{\kw{int} i;}}
		\end{itemize}
	\item[Definition] tells us about the value of a thing. \begin{itemize}
		\item \inlineexample{\code{i = 1;}}
		\end{itemize}
	\item[Signature of a procedure] the declaration of the procedure. It is composed of a return type, an identifier, and a collection of parameter declarations.
	\end{description}
	\begin{example}\smaller In C++: \begin{itemize}
		\item a method is declared in a \code{.hpp} file.
		\item a method is defined in a \code{.cpp} file.
		\end{itemize}
	\end{example}
\end{frame}

\subsubsection{Environment and state}
\begin{frame}{Environment and State}
	The association of names with locations in memory (the store) and then with values is described by two mappings:
	\begin{description}
	\item[Environment] mapping from names to locations in the store.
	\item[State] mapping from locations in store to their values.
	\end{description}
	\vfill
	\begin{center}
	\includegraphics[width=.9\linewidth]{environment_state}
	\end{center}
\end{frame}

\begin{frame}[b,fragile]{Example of Environment and State}
	\begin{lstlisting}[language=C]
	...
	int i;         /* global i */
	...
	void f(...) {
	  int i;       /* local i */
	  ...
	  i = 3;       /* use of local i */
	  ...
	}
	...
	x = i + 1;     /* use of global i */
	\end{lstlisting}
	\vfill
	\begin{center}
	\includegraphics[width=.9\linewidth]{environment_state_example}
	\end{center}
\end{frame}

\subsubsection{Static or dynamic policy}
\begin{frame}{Static or Dynamic Policy}
	\alertbox{One of the most important issues when designing a compiler is what decisions can the compiler make about the program.}
	\begin{block}{Static Policy}
	A program uses a policy that enables the compiler to decide an issue; \emph{the decision could be decided at compile time.}
	\end{block}
	\begin{block}{Dynamic Policy}
	The decision can be made when we execute the program; \emph{the decision is required at run time.}
	\end{block}
\end{frame}

\sidecite{Church.1941, Frege.1967, Wexelblat.1981}
\begin{frame}{Example of the Scope of the Declarations}
	\begin{itemize}
	\item A language uses a static scope if it is possible to determine the scope of a declaration by looking only at the program (C, Java\dots)
	\vfill
	\item With dynamic scope, as the program runs, the same use of a variable x could refer to any of several different declarations of x (Perl, PHP\dots).
	\end{itemize}
\end{frame}

\begin{frame}{Example of the use of the term ``static'' in Java}
	\begin{center}
		\code{\kw{public} \kw{static} \kw{int} x = 1;}
	\end{center}
	\vfill
	\begin{itemize}
	\item Here \kw{static} refers not to the scope of the variable, but rather to the ability of the compiler to determine the location in memory.
	\vfill
	\item If \kw{static} is omitted each object has this variable and the compiler cannot determine where it is in advance.
	\end{itemize}
\end{frame}

\begin{frame}{Are Environment and State Mappings Dynamic or Static?}
	\alertbox*{The environment and state mappings are often dynamic.}
	\begin{block}{Static or Dynamic Environment Mapping?}
	\begin{itemize}
	\item Most of binding names to locations are dynamic.
	\item Some declarations (\eg global \id{i}) are determine at compile time; they are static.
	\end{itemize}
	\end{block}
	\begin{block}{Static or Dynamic State Mapping?}
	\begin{itemize}
	\item Most of binding locations to values are dynamic because it is impossible to determine the location until we run the program.
	\item Declared constants are an exception.
	\end{itemize}
	\end{block}
\end{frame}

\subsubsection{Parameter-passing mechanisms}
\begin{frame}{\subsubsecname}
	\begin{itemize}
	\item All programming languages have the notion of procedure.
	\item But they can differ in how these procedures get their arguments.
	\vfill
	\item How are the actual parameters (the parameters used in the call of a procedure) associated with the formal parameters (those used in the procedure definition)?
		\begin{enumerate}
		\item Call-by-value
		\item Call-by-reference
		\item Call-by-name
		\end{enumerate}
	\end{itemize}
\end{frame}

\begin{frame}{Passing Parameters with Call-by-value}
	\begin{itemize}
	\item The actual parameter is evaluated or copied.
	\item The value is placed in the location belonging to the corresponding formal parameter of the called procedure.
	\item Used in C and Java; and the default option in C++.
	\item All computation involving the formal parameters done by the called procedure is local to that procedure; and
	\item The actual parameters themselves cannot be changed.
	\end{itemize}
	\begin{alertblock}{Caution}
		In Java, all the object variables are references, or pointers, to the objects. Parameters are passed with the call-by-value policy, not the call-by-reference.
	\end{alertblock}
\end{frame}

\begin{frame}{Passing Parameters with Call-by-reference}
	\begin{itemize}
	\item The address of the actual parameter is passed to the callee as the value of the corresponding formal parameter.
	\vfill
	\item Uses of the formal parameter in the code of the callee are implemented by following the pointer to the location indicated by the caller.
	\vfill
	\item Changes to the formal parameter thus appear as changes to the actual parameter.
	\end{itemize}
\end{frame}

\begin{frame}{Passing Parameters with Call-by-name}
	\begin{itemize}
	\item It requires that the callee execute as if the actual parameter were substituted literally for the formal parameter in the code of the callee.
	\vfill
	\item Macro-functions in the C-family languages use this parameter passing mechanism.
	\end{itemize}
\end{frame}

\subsubsection{Aliasing mechanism}
\begin{frame}{\subsubsecname}
	\begin{itemize}
	\item \emph{Aliasing} is occurring when two formal parameters refer to the same location.
	\item Such variables are said to be aliases of one another.
	\vfill
	\item It is possible when references to the objects are passed by value (as in Java).
	\end{itemize}
\end{frame}

\section[Language processor]{What is a language processor?}

\tableofcontentslide[sectionstyle={show/shaded},subsectionstyle={hide/hide/hide},subsubsectionstyle={hide/hide/hide/hide}]

\begin{frame}{What is a Compiler?}
	\begin{itemize}
	\item Read a program in one language --- the source language.
	\item Translate it into an equivalent program in another language --- the target language.
	\vfill
	\item Report any errors in the source program that are detected during the translation process.
	\end{itemize}
	\vfill
	\begin{center}
		\includegraphics[width=.2\linewidth]{compiler_processor}
	\end{center}
\end{frame}

\begin{frame}{Running the program}
	If the target program is an executable machine-language program, it can then be called by the user to process inputs and produce outputs
	\vfill
	\begin{center}
		\includegraphics[width=.2\linewidth]{run_program}
	\end{center}
\end{frame}

\begin{frame}{What is an Interpreter?}
	\begin{itemize}
	\item A kind of language processor.
	\item Does not produce a target program.
	\item Directly execute the operations specified in the source program on inputs supplied by the user.
	\end{itemize}
	\vfill
	\begin{center}
		\includegraphics[width=.4\linewidth]{interpreter_processor}
	\end{center}
\end{frame}

\begin{frame}{What is an Hybrid Compiler?}
	\begin{itemize}
	\item Combine compilation and interpretation.
	\item Generate intermediate program in a platform-independent language.
	\item Execute the intermediate program in a platform-dependent virtual machine.
	\end{itemize}
	\vfill
	\begin{center}
		\includegraphics[width=.7\linewidth]{hybrid_processor}
	\end{center}
\end{frame}

\begin{frame}{Properties of the Language Processors}
	\begin{smaller}
	\begin{block}{Compiler \vs Interpreter}
	\begin{itemize}
	\item Compiler is faster than interpreter at mapping inputs to outputs.
	\item Interpreter gives better error diagnostics than compiler, because it execute the source program statement by statement (no code optimization).
	\end{itemize}
	\end{block}
	\begin{block}{Hybrid Compiler}
	\begin{itemize}
	\item Hybrid compiler enables to compile on one machine, and to execute the generated program on an other machine with a different low-level architecture than the initial machine, or across a network\dots
	\item In order to achieve faster processing, some hybrid compilers use just-in-time compilers to translate intermediate programs into machine language and avoid the interpretation, \eg the Oracle's Java Runtime Environment.
	\end{itemize}
	\end{block}
	\end{smaller}
\end{frame}

\begin{frame}<1>[t]{Toolchain}
	\begin{itemize}
	\item Several other programs may be required to create an executable target program.
	\item They compose the toolchain of the compiler.
	\end{itemize}
	\putat(40,-210){\includeanimatedfigure[width=.8\linewidth]{toolchain}}
\end{frame}

\begin{frame}<2>[t]{The Preprocessor in the Toolchain}
	\begin{smaller}
	\begin{block}{\smaller Goals}
	\begin{itemize}
	\item To collect the different files of the program's modules to compile.
	\item To expand shorthands, macros into statements.
	\end{itemize}
	\end{block}
	\end{smaller}
	\putat(40,-210){\includeanimatedfigure[width=.8\linewidth]{toolchain}}
\end{frame}

\begin{frame}<3>[t]{The Compiler in the Toolchain}
	\begin{smaller}
	\begin{block}{\smaller Goals}
	\begin{itemize}
	\item To produce an assembly-language program from the modified source program.
	\item Assembly-language is easier to produce and debug.
	\end{itemize}
	\end{block}
	\end{smaller}
	\putat(40,-210){\includeanimatedfigure[width=.8\linewidth]{toolchain}}
\end{frame}

\begin{frame}<4>[t]{The Assembler in the Toolchain}
	\begin{smaller}
	\begin{block}{\smaller Goals}
	\begin{itemize}
	\item To translate to a machine code that could be relocated in the code segment of the program.
	\item Code segment: the part of the memory where machine code is stored
	\end{itemize}
	\end{block}
	\end{smaller}
	\putat(40,-210){\includeanimatedfigure[width=.8\linewidth]{toolchain}}
\end{frame}

\begin{frame}<5>[t]{The Linker in the Toolchain}
	\begin{smaller}
	\begin{block}{\smaller Goals}
	\begin{itemize}
	\item To resolve external memory addresses, where the code in one file (library or object) may refer to a location in another file (library or object)
	\end{itemize}
	\end{block}
	\end{smaller}
	\putat(40,-210){\includeanimatedfigure[width=.8\linewidth]{toolchain}}
\end{frame}

\section{Process of a compiler}

\tableofcontentslide[sectionstyle={show/shaded},subsectionstyle={hide/hide/hide},subsubsectionstyle={hide/hide/hide/hide}]

\begin{frame}<9>{Process of a Compiler}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\begin{minipage}{.8\linewidth}
	\begin{block}{Analysis}
	\begin{itemize}
	\item The \emph{analysis} breaks up the source program into constituent pieces and imposes a grammatical structure to them.
	\item It detects if the source program is ill formed or semantically unsound.
	\item It collects informations about the source program and stores it in a data structure called symbol table.
	\item This part is often called the \emph{front end of the compiler}.
	\end{itemize}
	\end{block}
	\end{minipage}
\end{frame}

\begin{frame}<10>{Process of a Compiler}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\begin{minipage}{.8\linewidth}
	\begin{block}{Synthesis}
	\begin{itemize}
	\item The \emph{synthesis} constructs the desired target program from the intermediate representation and the information in the symbol table.
	\item This part is often called the \emph{back end of the compiler}.
	\end{itemize}
	\end{block}
	\end{minipage}
\end{frame}

\begin{frame}<2>{Lexical Analysis or Scanning}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\begin{minipage}{.8\linewidth}
	\begin{itemize}
	\item The lexical analyzer reads the stream of characters making up the source program.
	\item It groups the characters into meaningful sequences called lexemes.
	\item For each lexeme, the lexical analyzer produces as output a token of the following form; and passes it to the syntactic analyzer.
		\begin{center}
			\code{{\textless}token-name, attribute-value{\textgreater}}
		\end{center}
	\item The \code{token-name} is the identifier of the token.
	\item The \code{attribute-value} points to an entry in the symbol table for this token.
	\end{itemize}
	\end{minipage}
\end{frame}

\begin{frame}<2>{Example of Scanning}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\begin{small}
	\begin{center}
		\code{position = initial + rate * 60}
	\end{center}
	\begin{minipage}{.8\linewidth}
	\begin{itemize}
	\item \code{position} is a lexeme mapped into the token \code{{\textless}id,1{\textgreater}}; where \code{id} is an abstract symbol standing for identifier and \code{``1''} points to the symbol-table entry for position.
	\item \code{=} is a lexeme mapped into the token \code{{\textless}={\textgreater}}
	\item \code{initial} is a lexeme mapped into the token \code{{\textless}id,2{\textgreater}}
	\item \code{+} is a lexeme mapped into the token \code{{\textless}+{\textgreater}}
	\item \code{rate} is a lexeme mapped into the token \code{{\textless}id,3{\textgreater}}
	\item \code{*} is a lexeme mapped into the token \code{{\textless}*{\textgreater}}
	\item \code{60} is a lexeme mapped into the token \code{{\textless}number,60{\textgreater}}
	\end{itemize}
	\end{minipage}
	\end{small}
\end{frame}

\begin{frame}<2>{Example of Scanning}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\putat(0,-200){\includeanimatedfigure[width=.7\linewidth]{compiler_cycle_example}}
\end{frame}

\begin{frame}{Symbol Table}
	\begin{itemize}
	\item The symbol table is a data structure containing a record for each variable name, with fields for the attributes of the name.
	\vfill
	\item The symbol table is filled or used by the different stages of the compiler.
	\vfill
	\item The attributes may provide information about the storage allocated for a name, its type, its scope, the number and types of the formal parameters, the method of passing each argument, and the type returned.
	\vfill
	\item The data structure should be designed to allow the compiler to find the record for each name quickly and to store or retrieve data from that record quickly.
	\end{itemize}
\end{frame}

\begin{frame}{Symbol Table and Scope}
	\begin{itemize}
	\item Scopes are implemented by setting up a separate symbol table for each scope.
	\item The \emph{most-closely nested rule for blocks} permits to define a data structure, which is based on \emph{chained symbol tables}.
	\end{itemize}
	\vspace{2em}
	\begin{center}
		\includegraphics[width=.8\linewidth]{symbol_table_chain}
	\end{center}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{Simple Java Implementation of the Symbol Table Chain}
	\begin{lstlisting}[language=Java]
	/** Define the properties of a single symbol. */
	public class Symbol {
	    public final String lexeme;
	    public Type type;
	    public Address storagePosition;
	    public Symbol(String lexeme) { this.lexeme = lexeme; }
	}

	/** Define a symbol table. */
	public class SymbolTable {

	    /** Collection of the symbol in the current context. */
	    private final Map<String,Symbol> table = new TreeMap<String,Symbol>();

	    /** Reference to the symbol table that is associated to the enclosing scope. */
	    private final SymbolTable enclosingEnvironment;

	    /** Constructor. */
	    private SymbolTable(SymbolTable enclosingEnvironment) {
	        this.enclosingEnvironment = enclosingEnvironment;
	    }

	    //....
	\end{lstlisting}
	\framebreak
	\begin{lstlisting}[language=Java]
	    /** Declare a symbol in the current context. */
	    public void declare(String identifier, Symbol symbol) {
	        this.table.put(identifier, symbol);
	    }

	    /** Get the definition of a symbol in the current context,
	        or in an enclosing scope. */
	    public Symbol get(String identifier) {
	        SymbolTable e = this;
	        Symbol symbol;
	        while (e!=null) {
	            symbol = e.table.get(identifier);
	            if (symbol != null) {
	                return symbol;
	            }
	            e = e.enclosingEnvironment;
	        }
	        return null;
	    }

	    //....
	\end{lstlisting}
	\framebreak
	\begin{lstlisting}[language=Java]
	    /** Reference to the current symbol table.
	        The reference is initialized with the
	        root context (or the global context). */
	    private static SymbolTable current = new SymbolTable(null);

	    /** Replies the symbol table of the current context. */
	    public static SymbolTable getCurrent() {
	        return current;
	    }

	    /** Open a new context and create the corresponding
	        symbol table. */
	    public static void openContext() {
	        current = new SymbolTable(current);
	    }

	    /** Close the current context. */
	    public static void closeContext() {
	        if (current.enclosingEnvironment!=null) {
	            current = current.enclosingEnvironment;
	        }
	    }

	}
	\end{lstlisting}
\end{frame}

\begin{frame}<3>{Syntax Analysis}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\begin{minipage}{.8\linewidth}
	\begin{itemize}
	\item The syntax analyzer uses the tokens produced by the lexical analyzer to create an intermediate representation.
	\item A typical representation is a syntax tree: \begin{description}
		\item[node] operation in the program
		\item[children] parameters of the operation
		\end{description}
		\vspace{1em}
		\includegraphics[width=\linewidth]{syntax_tree_example}
		\vspace{1em}
	\item In real-life compilers, a context free grammar is used to describe the syntax of the language and to help to analyze this syntax.
	\end{itemize}
	\end{minipage}
\end{frame}

\begin{frame}<3>{Example of Syntax Analysis}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\putat(0,-200){\includeanimatedfigure[width=.7\linewidth]{compiler_cycle_example}}
\end{frame}

\begin{frame}<4>{Semantic Analysis}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\begin{minipage}{.8\linewidth}
	\begin{itemize}
	\item The semantic analyzer uses the syntax tree and the information in the symbol table to check the source program for semantic consistency with the language definition.
	\vfill
	\item It gathers type information and saves it in either the syntax tree and the symbol table.
	\vfill
	\item \emph{Type checking} is an important part of the semantic analyzer: the compiler checks that each operator has matching operands.
	\vfill
	\item The semantic analyzer applies \emph{coercions}, or type conversions.
	\end{itemize}
	\end{minipage}
\end{frame}

\begin{frame}<4>{Example of Semantic Analysis}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\putat(0,-200){\includeanimatedfigure[width=.7\linewidth]{compiler_cycle_example}}
\end{frame}

\begin{frame}<5>{Intermediate Code Generator}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\begin{minipage}{.8\linewidth}
	\begin{itemize}
	\item Many compilers generate an explicit low-level or machine-like intermediate representation, which is a program for an abstract machine.
	\vfill
	\item The intermediate code generator should be: \begin{itemize}
		\item easy to produce, and
		\item easy to translate into the target machine.
		\end{itemize}
	\vfill
	\item Two intermediate representations are generally used: \begin{itemize}
		\item Syntax tree, and
		\item Three-address code
		\end{itemize}
	\end{itemize}
	\end{minipage}
\end{frame}

\begin{frame}{What is the Three-address Code?}
	\begin{itemize}
	\item Three-address code consists of a sequence of assembly-like instructions with, at most, three operands per instruction.
		\begin{center}
			\code{{\textless}variable{\textgreater} = {\textless}operand1{\textgreater} {\textless}operator{\textgreater} {\textless}operand2{\textgreater}}
		\end{center}
	\vfill
	\item Each operand can act like a register.
	\item The affectation operator is implicit and always present.
	\vfill
	\item \alert{Constraints}: \begin{enumerate}
		\item at most one operator on the right side.
		\item temporary names are generated to hold the value computed by the three-address instruction.
		\item some instructions have fewer then three operands.
		\end{enumerate}
	\end{itemize}
\end{frame}

\begin{frame}<5>{Example of Intermediate Code Generation}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\putat(0,-200){\includeanimatedfigure[width=.7\linewidth]{compiler_cycle_example}}
\end{frame}

\begin{frame}<6>{Machine-Independent Code Optimizer}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\begin{minipage}{.8\linewidth}
	\begin{itemize}
	\item The code optimizer improves the intermediate code so that better target code will result.
	\item ``Better'' means faster, shorter, less power consumer\dots
	\item All the compilers include a machine-independent code optimizer.
	\item Those that spent a large amount of time on this phase are named ``optimizing compilers.''
	\item Remember that many of the simple optimizations permit to significantly improve the running time of the target program without too much time spent on this phase.
	\end{itemize}
	\end{minipage}
\end{frame}

\figureslide{Example of Optimization}{intermediate_code_optim}

\begin{frame}<6>{Example of Machine-Independent Code Optimization}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\putat(0,-200){\includeanimatedfigure[width=.7\linewidth]{compiler_cycle_example}}
\end{frame}

\begin{frame}<7>{Code Generator}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\begin{minipage}{.8\linewidth}
	\begin{itemize}
	\item The code generator takes as input an intermediate representation and maps it into the target language.
	\vfill
	\item If the target language is machine code, registers or memory locations are selected for each variables used by the program.
	\item Then, the intermediate instructions are translated into sequences of machine instructions that perform the same tasks.
	\vfill
	\item A crucial aspect is the judicious assignment of registers to hold variables.
	\end{itemize}
	\end{minipage}
\end{frame}

\begin{frame}{Example of Code Generation}
	\begin{itemize}
	\item Assumes that R1 and R2 are registers.
	\item Variables are mapped to registers so that they can be easily used for the generation of the next instructions.
	\end{itemize}
	\vfill
	\includegraphics[width=\linewidth]{code_generator_example}
\end{frame}

\begin{frame}<7>{Example of Code Generation}
	\putat(270,-207){\includeanimatedfigure[width=.2\linewidth]{compiler_structure}}
	\putat(0,-200){\includeanimatedfigure[width=.7\linewidth]{compiler_cycle_example}}
\end{frame}

\section{Tools to create a compiler}

\tableofcontentslide[sectionstyle={show/shaded},subsectionstyle={hide/hide/hide},subsubsectionstyle={hide/hide/hide/hide}]

\begin{frame}[allowframebreaks]{Tools to Create a Compiler}
	\begin{itemize}
	\item Several tools are available to help the compiler writer to build his compiler.
	\item These tools use specialized languages for specifying and implementing specific components.
	\end{itemize}
	\begin{block}{Tools}\smaller
	\begin{description}
	\item[Parser generators] produce syntax analyzers from grammatical description of a programming language (Yacc, JavaCC, Bison\dots)
	\item[Scanner generators] produce lexical analyzers from a regular-expression description of the tokens of the languages (Flex, JFlex\dots)
	\item[Syntax-directed translation engines] produce collections of routines for walking a parse tree and generating intermediate code.
	\end{description}
	\end{block}
	%
	\framebreak
	%
	\begin{block}{Tools}\smaller
	\begin{description}
	\item[Code-generator generators] produce a code generator from a collection of rules for translating each operation of the intermediate language into the machine language for a target machine.
	\item[Data-flow analysis engines] facilitate the gathering of information about how values are transmitted from one part of the program to each other part.
	\item[Compiler-construction toolkits] provide an integrated set of routines for constructing various phases of a compiler (xtext\dots)
	\end{description}
	\end{block}
\end{frame}

\section{Conclusion}

\tableofcontentslide[sectionstyle={show/shaded},subsectionstyle={hide/hide/hide},subsubsectionstyle={hide/hide/hide/hide}]

\begin{frame}[t,allowframebreaks]{Key Concepts of the Chapter}
	\begin{description}
	\item[Language Processors] An integrated software development environment: compilers, interpreters, linkers, loaders, debuggers, profilers.
	\item[Compiler Phases] Sequence of phases, each of which transforms the source program from one intermediate representation to another.
	\item[Machine and Assembly Languages] Machine languages were the first-generation programming languages, followed by assembly languages.
	\item[Code Optimization] the science of improving the efficiency of code in both complex and very important. It is a major portion of the study of compilation.
	\item[Higher-Level Languages] Programming languages take on progressively more of the tasks that formerly were left to the programmer: memory management, type-consistency\dots
	\item[Environments] The association of names with locations in memory and then with values can be described in terms of environments.
	\item[Parameter Passing] Parameters are passed from a calling procedure to the callee either by value or by reference.
	\item[Aliasing] When parameters are (effectively) passed by reference, two formal parameters can refer to the same object.
	\item[Compiler Front End] The part of the compiler that is dedicated to the analysis phases. The compiler front end takes the source program, breaks it to token, analyzes the grammar, detects errors and inconsistencies, and generate an intermediate representation.
	\item[Compiler Back End] The part of the compiler that is dedicated to the synthesis phases. The compiler back end takes the intermediate representation, generates assembly and machine code.
	\item[Lexical Analyzer] The lexical analyzer reads the input one character at a time and produces as output a stream of tokens. A token consists of a terminal symbol and attribute values.
	\item[Parsing] Parsing is the problem of figuring out how a string of terminals can be derived from the start symbol of the grammar by repeatedly replacing a nonterminal by the body of one of its productions.
	\item[Parse Tree] A graphical tree representation of the productions that are matching a sequence of input tokens.
	\item[Intermediate Code] The result of the syntax analysis is a representation of the source program, called intermediate code. Two primary forms of intermediate code are illustrated: abstract syntax tree (similar to parse tree), and three-address code.
	\item[Symbol Table] A data structure that holds information about identifiers.
	\end{description}
\end{frame}

\begin{frame}[t,allowframebreaks]{\bibname\ of the Chapter}%
	\tiny%
	\putbib[bibliographies/chapter1]%
\end{frame}%

\end{bibunit}
